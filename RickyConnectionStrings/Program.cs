﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RickyConnectionStrings
    {
    /*
     Data Source=C:\Users\Tim\VS-Projects\RickyConnectionStrings\RickyConnectionStrings\bin\Debug\RickyConnectionStrings.Database.sdf
     */
    class Program
        {
        static void Main(string[] args)
            {
            var db = new Database();
            var everyone = db.People;
            Console.WriteLine("There are {0} people in the database", everyone.Count());
            Console.WriteLine("Adding another person...");
            var newperson = new Person() {Name = "Joe Bloggs", Email = "Me@nowhere.com"};
            everyone.Add(newperson);
            db.SaveChanges();
            Console.WriteLine("Changes saved - press ENTER");
            Console.ReadLine();
            }
        }
    }
