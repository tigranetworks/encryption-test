﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RickyConnectionStrings
    {
    class Person
        {
        public Person()
            {
            Id = Guid.NewGuid();
            }
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }
        }

    class Database : DbContext
        {
        public DbSet<Person> People { get; set; }
        }
    }
